import re

class Tokeniser:
	def __init__(self):
		self.punctuation = "[,.;:]"
		#self.ignore = '["()`«»´“”0-9]<>='
		self.word = "[a-zA-Z']+"
		self.parseRE = re.compile("(?P<punctuation>{0})|(?P<word>{1})".format(self.punctuation, self.word))
	
	def tokenise(self, stream):
		for match in self.parseRE.finditer(stream):
			if match.lastgroup in ("word", "punctuation"):
				yield match.group(match.lastgroup)
