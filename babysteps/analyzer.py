from __future__ import division
from collections import Counter


def get_shingles(tokens, length):
    tokens = list(tokens)
    for i in range(len(tokens) - length - 1):
        yield tokens[i:i+length], tokens[i+length]


def get_shingle_dict(shingles):
    shingle_dict = {}
    for shingle, prediction in shingles:
        shingle = tuple(shingle)
        shingle_dict.setdefault(shingle, [])
        shingle_dict[shingle].append(prediction)
    return shingle_dict


def get_shingle_probability(shingle_dict):
    for shingle, predictions in shingle_dict.items():
        counted = Counter(predictions)
        probabilities = {token: times/len(predictions) for token, times in
                         counted.items()}
        yield shingle, probabilities


def analyze(tokens, length=3):
    shingles = get_shingles(tokens, length)
    shingle_dict = get_shingle_dict(shingles)
    probabilities = get_shingle_probability(shingle_dict)
    return dict(probabilities)
